from django.conf.urls import url

from . import views

app_name = 'calculator'
urlpatterns = [
    url(r'^$', views.home, name='home'),
    url(r'^result/$', views.result, name='result'),
    #url(r'^total/$', views.totalvote, name='totalvote'),
    
]
