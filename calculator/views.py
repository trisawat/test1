from django.http import HttpResponse
from django.shortcuts import render

def home(request):
    return render(request,'calculator/home.html')

def result(request):
    num1 = int(request.POST['input1'])
    num2 = int(request.POST['input2'])
    operator = request.POST['enter']
    answer = ''
    if operator == '+':
        answer = num1 + num2 
    if operator == '-':
        answer = num1 - num2
    if operator == '*':
        answer = num1 * num2
    if operator == '/':
        answer = num1 / num2
        
          
    return render(request, 'calculator/result.html',{'num1':num1,'num2':num2,'operator':operator,'answer':answer})

